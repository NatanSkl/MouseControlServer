import socket

UDP_IP = "192.168.1.12"
UDP_PORT = 6117

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp.sendto(bytearray([10, 20]), (UDP_IP, UDP_PORT))

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.connect(("127.0.0.1", 6118))
tcp.send("C")
tcp.send("C")