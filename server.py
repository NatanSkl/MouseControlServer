import socket
import pyautogui
import ctypes
from multiprocessing import Pool
import select

IP = ""
UDP_PORT = 6117
TCP_PORT = 6118

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
udp.bind((IP, UDP_PORT))

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcp.bind((IP, TCP_PORT))
tcp.setblocking(0)
tcp.listen(5)

inputs = [udp, tcp]


def move_mouse(x_change, y_change):
    x, y = pyautogui.position()
    pyautogui.moveTo(x + ctypes.c_byte(x_change).value, y + ctypes.c_byte(y_change).value)


def read_udp(pool):
    data, addr = udp.recvfrom(2)
    array = bytearray(data)
    print "Received message: ", ctypes.c_byte(array[0]).value, ctypes.c_byte(array[1]).value
    pool.apply_async(move_mouse, [array[0], array[1]])


def read_tcp(data):
    x, y = pyautogui.position()
    if data == "L":
        pyautogui.click(x, y)
    elif data == "R":
        pyautogui.click(x, y, button="right")


if __name__ == "__main__":
    pool = Pool(processes=100)
    while inputs:
        readable, writeable, exceptional = select.select(inputs, [], inputs)
        for s in readable:
            if s is udp:
                read_udp(pool)
            elif s is tcp:
                conn, addr = s.accept()
                conn.setblocking(0)
                inputs.append(conn)
                print "Connected"
            else:
                try:
                    data = s.recv(1)
                except socket.error:
                    pass
                if data:
                    print data
                    read_tcp(data)
                """else:
                    print "Closing TCP connection"
                    inputs.remove(s)
                    s.close()"""
        for s in exceptional:
            print "Closing bugged stream"
            inputs.remove(s)
            s.close()

